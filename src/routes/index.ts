import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";
import { TpLinkPowerController } from "../services/tpLinkController";

/**
 * / route
 *
 * @class IndexRoute
 */
export class IndexRoute extends TpLinkPowerController{

  /**
   * @class IndexRoute
   * @constructor
   */
  constructor() {
    super();
  }

  /**
   * Create the routes.
   *
   * @class IndexRoute
   * @method create
   * @static
   */
  public static create(router: Router) {

    router.get("/", (req: Request, res: Response, next: NextFunction) => {
      new IndexRoute().index(req, res, next);
    });

    router.get("/tpLink/power/on", (req: Request, res: Response) => {
      new IndexRoute().TpLinkPowerState(true, req, res);
    });

    router.get("/tpLink/power/off", (req: Request, res: Response) => {
      new IndexRoute().TpLinkPowerState(false, req, res);
    });
  }
  
  /**
   * @class IndexRoute
   * @method tpLinkControl
   * @param {boolean} status desired power state.
   */
  private TpLinkPowerState (status: boolean, req: Request, res: Response, next?: NextFunction) { 
    try {

      this.PowerState(status)
      .then(() => {
        res.sendStatus(200);
      });

    } catch(err) {
      console.log(err);
      res.sendStatus(404);
    }
  }

  /**
   * @class IndexRoute
   * @method index
   * @param req {Request} The express Request object.
   * @param res {Response} The express Response object.
   * @param next {NextFunction} Execute the next method.
   */
  public index(req: Request, res: Response, next: NextFunction) {

    this.title = "Typescript Express Boilerplate";

    let options: Object = {
      "message": "Welcome to Express, NodeJS and Typescript2"
    };

    this.render(req, res, "index", options);
  }

}
