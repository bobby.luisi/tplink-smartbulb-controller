import { NextFunction, Request, Response } from "express";
import { BaseRoute } from "../routes/route";
import { Client } from "tplink-smarthome-api";

/**
 * @class TpLinkPowerController
 */

export class TpLinkPowerController extends BaseRoute {
    
    public powerStatus: boolean;

    /**
     * @class TpLinkPowerController
     * @method PowerState
     * @param {boolean} status The power status. 
     */
    public PowerState (status: boolean, ipAddress?: string) {
        return new Promise(
            (resolve, reject) => {
              const client = new Client();
              client.startDiscovery()
              .on('device-new', (device) => {
                device.getSysInfo()
                .then(console.log)
                .catch((err) => reject)
                device.setPowerState(status === true)
                .then(resolve)
                .catch(reject)
              })
              .on('error', (err) => {
                reject(err)
              })
            })
    }

}